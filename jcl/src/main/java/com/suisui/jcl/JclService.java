package com.suisui.jcl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * 默认使用jul进行日志输出
 * log4j依赖+log4j.properties配置文件，使用log4j进行日志输出
 */
public class JclService {
    private Log log = LogFactory.getLog(JclService.class);
    public void testAPI(){
        //输出日志
        log.trace("trace");
        log.debug("debug");
        log.info("info");
        log.warn("warn");
        log.fatal("fatal");
    }
}
