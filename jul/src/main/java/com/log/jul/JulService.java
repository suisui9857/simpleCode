package com.log.jul;

import java.io.IOException;
import java.util.logging.*;


/**
 * @author wmh
 * @version 1.0 2022/7/11
 * @Description: 使用jul不需要额外的jar包，默认配置文件
 **/
public class JulService {
    //加载配置文件
    static LogManager logManager = LogManager.getLogManager();
    static{
        try {
            logManager.readConfiguration(JulService.class.getClassLoader().getResourceAsStream("logging.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 测试logger的父子关系
     */
    public void test2(){
        //所有logger默认的父级是java.util.logging.LogManager$RootLogger
        //logger父子通过名字来决定
        Logger logger1 = Logger.getLogger("a");
        Logger logger2 = Logger.getLogger("a.b");
        Logger logger3 = Logger.getLogger("a.b.c");
        System.out.println(logger3.getParent().getName());//a.b
        System.out.println(logger3.getParent()== logger2);//true
        System.out.println(logger2.getParent()== logger1);//true
        System.out.println(logger1.getParent());

        //设置handler
        ConsoleHandler handler = new ConsoleHandler();
        //设置logger3自己的handler，会有日志重复输出的问题
        logger3.addHandler(handler);
       // logger3.setUseParentHandlers(false);
        logger3.info("logger3.info");
    }

    /**
     *控制台是洋红色 System.err.parintln
     *默认打印出info后面的日志：默认配置的日志级别是Info
     * 级别的概念：ALL<。。。。<OFF
     */
    public void test1(){
        //继承配置的类，配置文件生效
        // Logger logger = Logger.getLogger("com.log.jul.level.abc");
        Logger logger = Logger.getLogger(JulService.class.getName());
        //设置logger级别
      logger.setLevel(Level.ALL);
        //获取到父级并设置所有handler(appender)级别
        Handler[] handlers = logger.getParent().getHandlers();
        for (Handler handler : handlers) {
            //设置handler级别
            handler.setLevel(Level.ALL);
        }

        logger.log(Level.FINEST,"极好的");
        logger.log(Level.FINER,"挺好的的");
        logger.log(Level.FINE,"好的");
        logger.log(Level.CONFIG,"配置");
        logger.log(Level.INFO,"普通信息");
        logger.log(Level.WARNING,"警告");
        logger.log(Level.SEVERE,"严重的");
        logger.warning("警告的简单写法");
    }
}