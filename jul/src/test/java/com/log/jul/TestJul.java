package com.log.jul;


import org.junit.Test;

/**
 * @author wmh
 * @version 1.0 2022/7/11
 * @Description:
 **/
public class TestJul {

    @Test
    public void test1(){
        JulService service = new JulService();
        service.test1();
        System.out.println("111");//黑色
        System.err.println("111");//洋红色
    }

    @Test
    public void test2(){
        JulService service = new JulService();
        service.test2();
    }
}