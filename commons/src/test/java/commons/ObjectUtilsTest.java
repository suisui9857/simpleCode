package commons;

import org.apache.commons.lang3.ObjectUtils;
import org.junit.Test;

/**
 * @author wmh
 * @version 1.0 2022/6/7
 * @Description: 对象类测试
 **/
public class ObjectUtilsTest {

    //取第一个不为空的对象
    @Test
    public void test1(){
        String str1 = null;
        String str2 = null;
        String str3 = "  ";
        String str4 = "数字";
        System.out.println(ObjectUtils.firstNonNull(str1,str2,str3,str4));
    }

    //不管obj所在的类有没有重写hashCode方法
        //identityToString始终调用的都是Object.hashCode()方法--可以当作一个地址，重复率低
    @Test
    public void test2(){
        System.out.println(ObjectUtils.identityToString("abc"));
    }
}