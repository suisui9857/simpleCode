package commons;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;


import java.util.*;

/**
 * @author wmh
 * @version 1.0 2022/6/7
 * @Description: 集合测试
 **/
public class MapUtisTest {

    //list set不为null/size>0
    @Test
    public void test(){
        List<Object> list = new ArrayList<>();
        System.out.println(CollectionUtils.isEmpty(list));
        Set<Object> set = new TreeSet<>();
        System.out.println(CollectionUtils.isNotEmpty(set));

        Map<Object, Object> hashMap = new HashMap<>();
        System.out.println(MapUtils.isEmpty(hashMap));
        System.out.println(MapUtils.isNotEmpty(hashMap));
        //获取map中key为a的值，并转为integer
        System.out.println(MapUtils.getInteger(hashMap, "a"));
    }

    //获取交集，并集，差集
    @Test
    public void test2(){
        List<Object> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");

        List<Object> list1 = new ArrayList<>();
        list1.add("1");
        list1.add("2");
        list1.add("c");

        //交集
        System.out.println(CollectionUtils.intersection(list, list1));
        //并集
        System.out.println(CollectionUtils.union(list, list1));
        //差集
        System.out.println(CollectionUtils.subtract(list, list1));
        System.out.println(CollectionUtils.subtract(list1, list));

    }

    //判断数组是否为空
    @Test
    public void test3(){
        Integer[] ints = new Integer[1];
        Integer[] ints1 = new Integer[0];
        Integer[] ints2 = new Integer[]{};
        Integer[] ints3 = null;
        System.out.println(ArrayUtils.isEmpty(ints));//false
        System.out.println(ArrayUtils.isEmpty(ints1));//true
        System.out.println(ArrayUtils.isEmpty(ints2));//true
        System.out.println(ArrayUtils.isEmpty(ints3));//true
    }

    //
    @Test
    public void test4(){
        Integer[] ints = new Integer[1];
        ints[0] = 3;
        //ArrayUtils.toString打印数组内容
        System.out.println(ArrayUtils.toString(ints));//{3}
        //添加数据，新创建一个数组
        Integer[] ints1 = ArrayUtils.add(ints, 7);
        System.out.println(ArrayUtils.toString(ints1));//{3,7}
    }

}