package commons;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.util.FileCopyUtils;


import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;


/**
 * @author wmh
 * @version 1.0 2022/6/8
 * @Description: 文件测试类
 **/
public class FileUtilsTest {

    /**
     * 将文件内容转成字符串读出
     *
     * 文件内容按行读出
     * */
    @Test
    public void test() throws Exception {
        String path = "D:\\java\\idea\\IdeaProject2\\utils-demo\\src\\main\\resources\\1.txt";
        //String file = FileUtils.readFileToString(new File(path), String.valueOf(StandardCharsets.UTF_8));
        List list = FileUtils.readLines(new File(path), String.valueOf(StandardCharsets.UTF_8));
        System.out.println(list.size());
    }

    /**
     * 获取文件的baseName
     * 获取文件的后缀
     * @throws Exception
     */
    @Test
    public void test2() throws Exception {
        String path = "D:\\java\\idea\\IdeaProject2\\utils-demo\\src\\main\\resources\\1.txt";
        System.out.println(FilenameUtils.getBaseName(path));
        System.out.println(FilenameUtils.getExtension(path));
    }


    //FileCopyUtils不用关心流关闭
    @Test
    public void test3() throws Exception {
        //获取类路径下的资源
        ClassPathResource resource = new ClassPathResource("files/1.txt");
        //设置编码
        EncodedResource encodedResource = new EncodedResource(resource, StandardCharsets.UTF_8);
        String targetPath = "D:\\java\\idea\\IdeaProject2\\utils-demo\\src\\main\\resources\\files\\2.txt";
        //复制文件1的内容到文件2
        FileCopyUtils.copy(encodedResource.getInputStream(),new FileOutputStream(targetPath));



    }
}