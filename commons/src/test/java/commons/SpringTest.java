package commons;

import org.junit.Test;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

/**
 * @author wmh
 * @version 1.0 2022/6/8
 * @Description: Spring测试类
 **/
public class SpringTest {
    //判断当前应用是否加载了com.road.java.Zhao
    //当某个类存在后再做后续操作
    @Test
    public void test1(){
      String className = "com.road.java.Zhao";
        /*  try {
            // Class.forName(className);
             //SpringTest.class.getClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/
        //未加载返回fasle
        System.out.println(ClassUtils.isPresent(className, null));

    }

    //Spring对反射的操作
    @Test
    public void test2(){
        //获取到本类以及所有的父类，(包含父类的父类)的所有属性
        ReflectionUtils.doWithFields(person.class,field -> System.out.println(field.getName()));
        //获取到本类以及所有的父类，(包含父类的父类)的所有方法
        ReflectionUtils.doWithMethods(person.class,method -> System.out.println(method.getDeclaringClass().getName()+"的"+method.getName()));

    }


    @Test
    public void test3(){}

    @Test
    public void test4(){}
}