package commons;

/**
 * @author wmh
 * @version 1.0 2022/6/8
 * @Description:
 **/
public class person {
    private Long id;
    private String name;

    public void say(){
        System.out.println("hello!!");
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public person(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}