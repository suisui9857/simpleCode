package commons;

import jdk.management.resource.internal.inst.SocketOutputStreamRMHooks;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

/**
 * @author wmh
 * @version 1.0 2022/6/7
 * @Description: String测试类
 **/
public class StringUtilsTest {

    /**
     * 判断是否为空
     */
    @Test
    public void test1(){
        //判断是否为空 null/""/"  "
        String str = " ";
        System.out.println(StringUtils.isBlank(str));
        //判断不是空
        System.out.println(StringUtils.isNotBlank(str));

        System.out.println("====================");
        //判断是否是null/""
        //" "不为empty
        String str1 = "";
        System.out.println(StringUtils.isEmpty(str1));
        //判断不是空
        System.out.println(StringUtils.isNotEmpty(str1));
    }

    /**
     * 手机号脱敏
     */
    @Test
    public void test2(){
        String str = "12345678945";
        //返回某个字符串左边的几个字符
        String left = StringUtils.left(str, 3);
        String right = StringUtils.right(str, 4);
        System.out.println(left+"****"+right);
        //rightPad：如果left长度<7，就在右边用*填充到7个长度
        String leftPad = StringUtils.rightPad(left, 7, "*");
        System.out.println(leftPad+right);
    }
}