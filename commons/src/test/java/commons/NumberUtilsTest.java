package commons;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;

/**
 * @author wmh
 * @version 1.0 2022/6/7
 * @Description: 数字测试类
 **/
public class NumberUtilsTest {

    //判断一个参数是不是数字
    @Test
    public void test1(){

        //isDigits，只能包含整数
        //isParsable：整数，小数都可,不能识别正负
        //isCreatable可以识别整数，浮点数，正负号，进制
        String str = "12.3aa";
        System.out.println(NumberUtils.isDigits(str));//false
        System.out.println(NumberUtils.isParsable(str));//false
        System.out.println(NumberUtils.isCreatable(str));//false
        String str1 = "12.3";
        System.out.println(NumberUtils.isDigits(str1));//false
        System.out.println(NumberUtils.isParsable(str1));//true
        System.out.println(NumberUtils.isCreatable(str1));//true
        String str2 = "12";
        System.out.println(NumberUtils.isDigits(str2));//true
        System.out.println(NumberUtils.isParsable(str2));//true
        System.out.println(NumberUtils.isCreatable(str2));//true
        String str3 = "+12";
        System.out.println(NumberUtils.isDigits(str3));//false
        System.out.println(NumberUtils.isParsable(str3));//false
        System.out.println(NumberUtils.isCreatable(str3));//true
        String str4 = "09";
        System.out.println(NumberUtils.isDigits(str4));//true
        System.out.println(NumberUtils.isParsable(str4));//true
        System.out.println(NumberUtils.isCreatable(str4));//false--8进制

    }


}