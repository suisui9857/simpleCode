package commons;

import com.google.common.base.CaseFormat;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.*;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author wmh
 * @version 1.0 2022/6/8
 * @Description:
 **/
public class GuavaTest {

    /**
     * Joiner:把集合/数组/可变参数，通过指定的分隔符连接成字符串
     *
     */
    @Test
    public void test(){
        List<Object> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add(null);
        //skipNulls()忽略null
        Joiner on = Joiner.on(",").useForNull("null替代");//替换null
        System.out.println(on.join(list));

        //System.out.println(list.stream().filter(StringUtils::isNotBlank).collect(Collectors.joining(",")));

        String str = "a,b,\"\",  ,   c";
        //on指定字符串的分隔符
        Splitter on1 = Splitter.on(",");
        Iterable<String> split = on1.split(str);
        System.out.println(split);
        //过滤空白的字符串，不包括""
        Splitter on2 = Splitter.on(",").omitEmptyStrings();
        Iterable<String> split1 = on2.split(str);
        System.out.println(split1);
        //.trimResults空格不保留
        Splitter on3 = Splitter.on(",").omitEmptyStrings().trimResults();
        Iterable<String> split2 = on3.split(str);
        System.out.println(split2);
        //转数组
        Iterable<String> split3 = on3.splitToList(str);
        System.out.println(split3);
    }

    //下划线和驼峰命名法互转
    //student_name -->studentName
    @Test
    public void test1(){
        String str = "student_name";
        System.out.println(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, str));
        System.out.println(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, str));

        String str1 = "studentName";
        System.out.println(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, str1));
    }

    //Lists,Sets,maps的
    @Test
    public void test2(){
        List<Object> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        //提供集合的快速创建方式
        List<String> list1 = Lists.newArrayList("a", "b", "c","d");
       // Set<String> hashSet = Sets.newHashSet("a", "b", "c","d");

        //把list2分成小的集合，小的集合的大小是size
        List<List<String>> lists = Lists.partition(list1, 2);
        System.out.println(lists);
    }

    @Test
    public void test3(){
        List<Integer> list = Ints.asList(1, 2, 3);
        System.out.println(list);
        //Longs
    }

    /**
     * Multiset--可以放重复元素
     * List：元素可重复的有序集合
     * set：元素不可重复的无序集合
     */
    @Test
    public void test4(){
        HashMultiset<String> multiset = HashMultiset.create();
        multiset.add("a");
        multiset.add("b");
        multiset.add("c");
        multiset.add("a");
        System.out.println(multiset);//[a x 2, b, c]
        //各个元素出现的次数
        Set<Multiset.Entry<String>> entries = multiset.entrySet();
        for (Multiset.Entry<String> entry : entries) {
            System.out.println("元素"+entry.getElement()+",个数："+entry.getCount());
        }
       System.out.println(multiset.entrySet());//[a x 2, b, c]
       System.out.println(multiset.elementSet());//[a, b, c]
    }

    /**
     * HashMultiMap用来替代jdk原生的Map<String,Collection<String>> map
     */
    @Test
    public void test5(){
        Multimap<String, String> multimap = HashMultimap.create();
        multimap.put("a", "1");
        multimap.put("b", "2");
        Collection<String> a = multimap.get("a");
        System.out.println(multimap);//{a=[1], b=[2]}
        System.out.println(a);//[1]
        //是否包含key=a,value=1的entry
        System.out.println(multimap.containsEntry("a", "1"));

        //转换成jdk原生api实现的数据结构
        Map<String, Collection<String>> map = multimap.asMap();
        System.out.println(map);//{a=[1], b=[2]}
    }

    //不可变集合
    @Test
    public void test6(){
        List<String> list = new ArrayList<>();
        list.add("aa");
        list.add("bb");
        //不可变集合
        ImmutableList<Object> list1 = ImmutableList.builder().add("aa").build();
        //list1.add("cc");报错不可添加
        Collection<String> collection = Collections.unmodifiableCollection(list);
        //collection.add("cc");报错不可添加
        list.add("cc");//但是源头可添加--哈哈哈
        System.out.println(collection);
    }


    @Test
    public void test7(){
        String param = null;
        Integer param1 = 4;
//        if(param == null){
//            throw  new RuntimeException("参数不可为空！");
//        }
       // Preconditions.checkNotNull(param,"参数不能为空");
        //表达式为fasle抛出异常
        Preconditions.checkArgument(param1 <=5,"参数大于5");
    }

}