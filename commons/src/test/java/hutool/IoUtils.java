package hutool;

import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

/**
 * @author wmh
 * @version 1.0 2022/6/9
 * @Description: IO流
 **/
public class IoUtils {
    public static void main(String[] args) throws Exception {
        BufferedInputStream in = FileUtil.getInputStream("D:\\java\\idea\\IdeaProject2\\HutoolTest\\src\\main\\resources\\files\\1.txt");
        BufferedOutputStream out = FileUtil.getOutputStream("D:\\java\\idea\\IdeaProject2\\HutoolTest\\src\\main\\resources\\files\\2.txt");
        IoUtil.copy(in, out, IoUtil.DEFAULT_BUFFER_SIZE);//默认缓存大小1024

        FileTypeUtil.getType(FileUtil.file("class"));

    }
}