package hutool;


import cn.hutool.core.date.BetweenFormatter;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.Month;
import org.junit.Test;

import java.util.Date;

/**
 * @author wmh
 * @version 1.0 2022/6/9
 * @Description: 时间类
 **/
public class DateTest {

    @Test
    public void test1(){
        //当前时间
/*        Date date = DateUtil.date();//2022-06-09 11:51:32
        Date date2 = DateUtil.date(Calendar.getInstance());//2022-06-09 11:51:32
        Date date3 = DateUtil.date(System.currentTimeMillis());//2022-06-09 11:51:32
        String now = DateUtil.now();//2022-06-09 11:51:32
        String today= DateUtil.today();//2022-06-09*/

        //字符串转日期
        String dateStr = "2017-03-01";
        Date date = DateUtil.parse(dateStr);//2017-03-01 00:00:00
        String dateStr1 = "2017-03-01 15:55:55";
        Date date1 = DateUtil.parse(dateStr1);//2017-03-01 15:55:55
        //日期转格式
        String format = DateUtil.format(date, "yyyy/MM/dd");//2017/03/01

        //获取年月日
        String formatDate = DateUtil.formatDate(date);//2017-03-01
        //获取年月日时
        String formatDateTime = DateUtil.formatDateTime(date);//2017-03-01 00:00:00
        //获取时间
        String formatTime = DateUtil.formatTime(date);//00:00:00
        //获得年的部分
        int year = DateUtil.year(date);//2017
        //获得月份，从0开始计数
        int month1 = DateUtil.month(date);//2
        Month month = DateUtil.monthEnum(date);//MARCH
        Date beginOfDay = DateUtil.beginOfDay(date);//一天的开始，结果：2017-03-01 00:00:00
        Date endOfDay = DateUtil.endOfDay(date);//一天的结束，结果：2017-03-01 23:59:59

        //日期时间差
        String dateStr01 = "2017-03-01 22:33:23";
        Date date01 = DateUtil.parse(dateStr01);

        String dateStr02 = "2017-04-01 23:33:23";
        Date date02 = DateUtil.parse(dateStr02);
        long between = DateUtil.between(date01, date02, DateUnit.DAY);//相差时间31天
        DateUtil.lastMonth();//上月的此时此刻
        //Level.MINUTE表示精确到分 毫秒数
        DateUtil.formatBetween(85333355, BetweenFormatter.Level.MINUTE);

        // 星座和生肖
         DateUtil.getZodiac(Month.JUNE.getValue(), 1);//双子座
         DateUtil.getChineseZodiac(1998);//虎

        //年龄，是否闰年
        DateUtil.ageOfNow("1990-01-30");
        DateUtil.isLeapYear(2017);
    }
}