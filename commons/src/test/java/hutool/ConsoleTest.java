package hutool;



import cn.hutool.core.lang.Console;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author wmh
 * @version 1.0 2022/6/8
 * @Description:
 **/
public class ConsoleTest {
    public static void main(String[] args) {
        Console.log("打印");//打印
        String[] arr = {"abc","cds","sd"};
        Console.log(arr);//[abc, cds, sd]
        ArrayList<Object> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        Console.log(list);//[a, b, c]
        HashMap<Object, Object> map = new HashMap<>();
        map.put("k1","v1");
        map.put("k2","v2");
        Console.log(map);//{k1=v1, k2=v2}
    }
}