package com.suisui.slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slf4jService {
    /**
     * slf4j+logback 引入依赖slf4j+logback配置文件
     * slf4j+log4j 切换jar包+log4j.properties配置文件，删除logback配置文件
     */
    //默认会去类路径下找名为logback.xml的配置文件
    private static final Logger logger = LoggerFactory.getLogger(Slf4jService.class);

    public void testAPI(){
        //输出日志
        logger.trace("trace信息");
        logger.debug("debug");
        logger.info("info信息");
        logger.warn("warn");
        logger.error("error");
    }
}
