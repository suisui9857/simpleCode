package com.beans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wmh
 * @version 1.0 2022/7/6
 * @Description:
 **/
@SpringBootApplication
public class MapStructApplication {
    public static void main(String[] args) {
        SpringApplication.run(MapStructApplication.class);
    }
}