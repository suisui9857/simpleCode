package com.beans.vo;

import lombok.Data;

/**
 * @author wmh
 * @version 1.0 2022/7/6
 * @Description:
 **/
@Data
public class PartVO {
    /**
     * 汽车零件id
     */
    private Long partId;
    /**
     * 零件名字
     */
    private String partName;
}