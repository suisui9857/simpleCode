package com.beans.vo;

import lombok.Data;

/**
 * @author wmh
 * @version 1.0 2022/7/6
 * @Description:
 **/
@Data
public class DriverVO {
    /**
     * 驾驶员id
     */
    private Long driverId;
    /**
     * 驾驶员名称
     */
    private String fullName;
}