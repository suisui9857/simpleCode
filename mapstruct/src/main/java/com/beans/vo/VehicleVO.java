package com.beans.vo;

import lombok.Data;

/**
 * @author wmh
 * @version 1.0 2022/7/8
 * @Description:
 **/
@Data
public class VehicleVO {
    /**
     * 编号
     */
    private Long id;
    /**
     * 裸车的价格
     */
    private Double price;
    /**
     * 品牌
     */
    private String brandName;
}