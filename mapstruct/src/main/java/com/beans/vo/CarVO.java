package com.beans.vo;


import lombok.Data;


/**
 * @author wmh
 * @version 1.0 2022/7/6
 * @Description:
 **/
@Data
public class CarVO {
    /**
     * 编号
     */
    private Long id;
    /**
     * 车辆编号
     */
    private String vin;
    /**
     * 裸车价格
     */
    private double price;
    /**
     * 上路价格，保留两位小数
     */
    private String totalPrice;
    /**
     * 生产日期
     */
    private String publishDate;
    /**
     * 品牌名称
     */
    private String brandName;
    /**
     * 汽车是否包含零件
     */
    private Boolean hasPart;
    /**
     * 骑车的司机
     */
    private DriverVO driverVO;

    /**
     * 车的颜色
     */
    private String color;
}