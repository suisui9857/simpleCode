package com.beans.dto;

import lombok.Data;

/**
 * @author wmh
 * @version 1.0 2022/7/6
 * @Description:汽车零件
 **/
@Data
public class PartDTO {
    /**
     * 汽车零件id
     */
    private Long partId;
    /**
     * 汽车零件名字
     */
    private String partName;
}