package com.beans.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author wmh
 * @version 1.0 2022/7/6
 * @Description: 汽车DTO对象
 **/
@Data
public class CarDTO {
    /**
     * 编号
     */
    private Long id;
    /**
     * 车辆编号
     */
    private String vin;
    /**
     * 裸车价格
     */
    private double price;
    /**
     * 上路价格
     */
    private double totalPrice;
    /**
     * 生产日期
     */
    private Date publishDate;
    /**
     * 品牌名称
     */
    private String brand;
    /**
     * 汽车包含零件列表
     */
    private List<PartDTO> partDTOS;
    /**
     * 骑车的司机
     */
    private DriverDTO driverDTO;

    /**
     * 车的颜色
     */
    private String color;

}