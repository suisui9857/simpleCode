package com.beans.dto;

import lombok.Data;

/**
 * @author wmh
 * @version 1.0 2022/7/6
 * @Description: 驾驶员DTO对象
 **/
@Data
public class DriverDTO {
    /**
     * id
     */
    private Long id;
    /**
     * 驾驶员名称
     */
    private String name;
}