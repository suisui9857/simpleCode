import com.beans.MapStructApplication;
import com.beans.convert.CarConvert;
import com.beans.dto.CarDTO;
import com.beans.dto.DriverDTO;
import com.beans.dto.PartDTO;
import com.beans.vo.CarVO;
import com.beans.vo.DriverVO;
import com.beans.vo.VehicleVO;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author wmh
 * @version 1.0 2022/7/6
 * @Description:
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MapStructApplication.class)
public class MapStructTest {

    @Resource
    private CarConvert carConvert;
    @Test
    public void test6() {
        VehicleVO vehicleVO = new VehicleVO();
        vehicleVO.setId(999L);
        vehicleVO.setBrandName("别克");
        vehicleVO.setPrice(65899544d);
        //CarDTO dto = CarConvert.INSTANCE.vehicleVO2carDTO(vehicleVO);
        CarDTO dto = carConvert.vehicleVO2carDTO(vehicleVO);
        System.out.println(dto);
    }
    @Test
    public void test4() {
        CarDTO carDTO = buildCarDTO();
        VehicleVO vehicleVO = CarConvert.INSTANCE.carDTO2vehicleVO(carDTO);
        System.out.println(vehicleVO);
        CarDTO carDTO2 = new CarDTO();
        //测试@InheritConfiguration继承配置
        //通过carDTO2的属性值来更新已存在的vehicleVO对象
        carDTO2.setBrand("迈巴赫");
        VehicleVO updateVehicleVO = CarConvert.INSTANCE.updateVehicleVO(carDTO2, vehicleVO);
        System.out.println(updateVehicleVO);
    }
    /**
     * 批量
     */
    @Test
    public void test3() {
        CarDTO carDTO = buildCarDTO();
        List<CarDTO> carDTOList = new ArrayList<>();
        carDTOList.add(carDTO); //source
        carDTOList.add(carDTO);
        //target
      /*  ArrayList<CarVO> carVOList = new ArrayList<>();
        for (CarDTO dto : carDTOList) {
            CarVO carVo = CarConvert.INSTANCE.dto2vo(dto);
            carVOList.add(carVo);
        }*/
        List<CarVO> carVOS = CarConvert.INSTANCE.dtos2vos(carDTOList);
        System.out.println(carVOS);
    }

    @Test
    public void test2() {
        CarDTO carDTO = buildCarDTO();
        CarVO carConvert = CarConvert.INSTANCE.dto2vo(carDTO);
        System.out.println(carConvert);
    }
    /**
     * 测试 通过getter和setter赋值完成pojo之间的转换
     */
    @Test
    public void test1(){
        //模拟业务构造出的carDTO对象
        CarDTO carDTO = buildCarDTO();
        //转换dto-vo
        CarVO carVO = new CarVO();
        carVO.setId(carDTO.getId());
        carVO.setVin(carDTO.getVin());
        carVO.setBrandName(carDTO.getBrand());

        carVO.setPrice(carDTO.getPrice());//装箱拆箱机制

        //double转string，保留两位小数
        double totalPrice = carDTO.getTotalPrice();
        DecimalFormat format = new DecimalFormat("#.00");
        String totalPriceStr = format.format(totalPrice);
        carVO.setTotalPrice(totalPriceStr);

        //日期格式转换
        Date publishDate = carDTO.getPublishDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(publishDate);
        carVO.setPublishDate(date);

        List<PartDTO> partDTOS = carDTO.getPartDTOS();
        boolean hasPart = partDTOS != null && !partDTOS.isEmpty();
        carVO.setHasPart(hasPart);

        DriverVO driverVO = new DriverVO();
        DriverDTO driverDTO = carDTO.getDriverDTO();
        driverVO.setDriverId(driverDTO.getId());
        driverVO.setFullName(driverDTO.getName());
        carVO.setDriverVO(driverVO);

    }

    /**
     * 模拟业务构造出的carDTO对象
     * @return
     */
    private CarDTO buildCarDTO() {
        CarDTO carDTO = new CarDTO();
        carDTO.setId(330L);
        carDTO.setVin("vin12345698");
        carDTO.setPrice(123789.685);
        carDTO.setTotalPrice(145698.665);
        carDTO.setPublishDate(new Date());
        PartDTO partDTO1 = new PartDTO();
        partDTO1.setPartId(1L);
        partDTO1.setPartName("多功能方向盘");
        PartDTO partDTO2 = new PartDTO();
        partDTO2.setPartId(2L);
        partDTO2.setPartName("智能车门");
        ArrayList<PartDTO> list = new ArrayList<>();
        list.add(partDTO1); list.add(partDTO2);
        carDTO.setPartDTOS(list);
        DriverDTO driverDTO = new DriverDTO();
        driverDTO.setId(2L);
        driverDTO.setName("哇哈");
        carDTO.setDriverDTO(driverDTO);
        carDTO.setColor("白色");
        carDTO.setBrand("品牌");
        return carDTO;
    }
}