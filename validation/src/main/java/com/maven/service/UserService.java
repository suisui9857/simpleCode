package com.maven.service;

import com.maven.util.ValidationUtil;

import javax.validation.constraints.NotBlank;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author wmh
 * @version 1.0 2022/7/3
 * @Description:
 **/
public class UserService {
    /**
     * 方法非bean类型的入参校验
     * 1.方法参数前加注解
     * 2.执行入参校验，真正有用的话可以使用aop编程思想来使用
     */

    //非bean入参，非bean返回值
    public String getByName(@NotBlank String name){
        //执行入参校验
        //当前线程的堆栈，第一个元素就是当前所在方法的名字
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
        String methodName = stackTraceElement.getMethodName();
        Method method = null;
        try{
            method = this.getClass().getDeclaredMethod(methodName, String.class);
        }catch (Exception e) {
            e.printStackTrace();
        }
        List<String> strings = ValidationUtil.validNotBean(this, method, new Object[]{name});
        //打印校验结果
        System.out.println("校验结果：" + strings);
        return "ok";
    }
}