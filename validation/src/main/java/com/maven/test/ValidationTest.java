package com.maven.test;

import com.maven.entity.Grade;
import com.maven.entity.UserInfo;
import com.maven.util.ValidationUtil;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author wmh
 * @version 1.0 2022/7/3
 * @Description: 校验方法
 **/
public class ValidationTest {
    public static void main(String[] args) {
        UserInfo info = new UserInfo();
        info.setName("名字");
        info.setAge(90);
        info.setEmail("129000099@qq.com");
        info.setPhone("15581829895");
        info.setBirthDay(LocalDateTime.now().minusDays(1));
        info.setPersonalPage("https://blog.csdn.net/zuo_kaizheng/article/details/121417185");
        //添加grade
        Grade grade = new Grade();
        //grade.setId("123");
       // info.setGrade(grade);
        info.setStatus(200);
        //假设新增,只校验add组，还得校验之前的默认组
       List<String> valid1 = ValidationUtil.valid(info);
        /*List<String> valid2 = ValidationUtil.valid(info,UserInfo.Update.class);
        //快速失败
        List<String> valid = ValidationUtil.validFailFast(info,UserInfo.Add.class);
*/
        System.out.println("valid1"+valid1);

    }
}