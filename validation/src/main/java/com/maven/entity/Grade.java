package com.maven.entity;

import javax.validation.constraints.NotBlank;

/**
 * @author wmh
 * @version 1.0 2022/7/3
 * @Description: 班级信息
 **/
public class Grade {

   // @NotBlank
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}