package com.maven.entity;

import com.maven.util.UserStatus;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;


import javax.validation.constraints.*;
import javax.validation.groups.Default;
import java.time.LocalDateTime;

/**
 * @author wmh
 * @version 1.0 2022/7/2
 * @Description: 用户信息
 **/
public class UserInfo{

    public interface Add extends Default {}
    public interface Update extends Default{}
    public interface Delete extends Default{}
    //默认组default
    @Null(groups = {Add.class}) //只适用于新增
    @NotNull(groups = {Update.class,Delete.class})//只适用于修改/删除
    private Long id;
    //@NotNull，只校验为null
    //@NotEmpty，校验null "",集合不为空
    @NotBlank(message = "姓名不能为空！！")// 校验null "" " "，只能作用字符串类型
    private String name;
    @NotNull
    @Min(1) @Max(800) //只有!= null的时候才才生效，【1，800】
    @Range(min = 1,max = 800,message = "年龄必须在1-800之间")
    //@Min(value = 18,message = "年龄小于{value}岁，禁止进入")
    private Integer age;
    @Email
    @NotBlank(message = "邮箱不能为空")
    private String email;
    //自己定义手机号的正则表达式
    @NotBlank
    @Pattern(regexp = "1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\\d{8}$")
    private String phone;
    @NotNull
    @Past//过期日期
    private LocalDateTime birthDay;
    @URL
    private String PersonalPage;
    @org.hibernate.validator.constraints.NotEmpty
    //@Valid //被引用对象加上@valid注解，才可以完成级联校验
    private Grade grade;

    @UserStatus
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDateTime getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDateTime birthDay) {
        this.birthDay = birthDay;
    }

    public String getPersonalPage() {
        return PersonalPage;
    }

    public void setPersonalPage(String personalPage) {
        PersonalPage = personalPage;
    }
}