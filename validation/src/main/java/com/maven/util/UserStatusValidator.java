package com.maven.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

/**
 * @author wmh
 * @version 1.0 2022/7/3
 * @Description: 用户状态约束
 **/
public class UserStatusValidator implements ConstraintValidator<UserStatus,Integer> {

    @Override
    public void initialize(UserStatus constraintAnnotation) {

    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null){
            //没有值，不校验
            return true;
        }
        Set<Integer> set = new HashSet<>();
        set.add(1000);
        set.add(1001);
        set.add(1002);
        return set.contains(value);
    }
}