package com.maven.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author wmh
 * @version 1.0 2022/7/3
 * @Description: 用户状态
 **/

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {UserStatusValidator.class})//说明当前注解要被谁来完成校验工作
@Documented
public @interface UserStatus {
    String message() default "{userStatus必须是1000/1001/1002}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}