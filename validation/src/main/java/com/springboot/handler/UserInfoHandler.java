package com.springboot.handler;

import com.maven.entity.UserInfo;
import com.maven.util.ValidationUtil;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wmh
 * @version 1.0 2022/7/3
 * @Description:
 **/
@RestController
@Validated//表示整个类都启用校验，如果碰到入参含有bean validation注解的话就会自动校验
public class UserInfoHandler {

    @GetMapping("/getByName")
    public String getByName(@NotBlank String name){
        return name;
    }

    /**
     * 编程式校验
     * @param userInfo
     * @return
     */
    @GetMapping("/addUser")
    public String getByName(UserInfo userInfo){
        List<String> valid = ValidationUtil.valid(userInfo);
        if (valid.size()>0){
            System.out.println(valid);
            return "校验不成功";
        }else {
        return "添加成功！";}
    }

    /**
     * 声明式校验，推荐
     * 直接加@Valid，有问题，直接抛出异常
     * 参数BindingResult，不会页面上报错
     * @param userInfo
     * @return
     */
    @GetMapping("/addUser2")
    public String getByName2(@Valid UserInfo userInfo, BindingResult result){
        if(result.hasErrors()){//判断是否有不满足约束的
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                System.out.println(error.getObjectName()+"::"+error.getDefaultMessage());
            }
            //获取没通过校验的字段详情
            List<FieldError> fieldErrorList = result.getFieldErrors();
            for (FieldError fieldError : fieldErrorList) {
                System.out.println(fieldError.getField() + "::" + fieldError.getDefaultMessage() + "，当前没有通过校验规则的值是：" + fieldError.getRejectedValue());
            }
        };
        return "添加成功";
    }


    /**
     * 测试分组校验
     * @param userInfo
     * @param result
     * @return
     */
    @GetMapping("/addUser3")
    public String getByName3(@Validated(value = {UserInfo.Add.class}) UserInfo userInfo, BindingResult result){
        if(result.hasErrors()){//判断是否有不满足约束的
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                System.out.println(error.getObjectName()+"::"+error.getDefaultMessage());
            }
            //获取没通过校验的字段详情
            List<FieldError> fieldErrorList = result.getFieldErrors();
            for (FieldError fieldError : fieldErrorList) {
                System.out.println(fieldError.getField() + "::" + fieldError.getDefaultMessage() + "，当前没有通过校验规则的值是：" + fieldError.getRejectedValue());
            }
        };
        return "添加成功";
    }

    @GetMapping("/addUser4")
    public String getByName4(@Validated(value = {UserInfo.Add.class}) UserInfo userInfo){
        return "添加成功！";
    }

    /**
     * 在每个controller里写上@ExceptionHandler，可以处理当前controller里面抛出的xx异常
     * @param e
     * @return
     */
  /*  @ExceptionHandler(BindException.class)
    public String handleEx(BindException e){
        List<FieldError> fieldErrors = e.getFieldErrors();
        StringBuilder builder = new StringBuilder();
        for (FieldError fieldError : fieldErrors) {
            builder.append("属性：").append(fieldError.getField()).append("校验不通过的原因：").append(fieldError.getDefaultMessage()).append(";;");
        }
        return builder.toString();
    }*/
}