package com.suisui.log4j;

import org.apache.log4j.Logger;

public class Log4jService {
    //所有的日志技术，都有父子关系概念，通过"."确定父子关系
    //com.suisui.log4j.Log4jService，获取logger
    private static Logger logger = Logger.getLogger(Log4jService.class.getName());

    /**
     * 要使用log4j，必须有log4j的配置文件，默认在类路径下找log4j.properties
     */
    public void testAPI(){
        //输出日志
        logger.trace("trace信息");
        logger.debug("debug");
        logger.info("info信息");
        logger.warn("warn");
        logger.fatal("fatal");
    }
}
